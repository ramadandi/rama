<?php
include ('cek.php');
include ('koneksi.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
      
      <header class="main-header">
        <!-- Logo -->
        <a class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Inventaris</b></span>
        </a>
        <ul class="nav pull-right">
                        
    </ul>
        <!-- Header Navbar: style can be found in header.less -->

              <!-- Control Sidebar Toggle Button -->
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Menu admin</p>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
          
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="inventaris.php"><i class="fa fa-circle-o"></i> Inventaris</a></li>
              </ul>
            </li>
 <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="peminjaman.php"><i class="fa fa-circle-o"></i> Peminjaman</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pengembalian.php"><i class="fa fa-circle-o"></i> Pengembalian</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="ruang.php"><i class="fa fa-circle-o"></i> Ruang</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pegawai.php"><i class="fa fa-circle-o"></i> Pegawai</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="jenis.php"><i class="fa fa-circle-o"></i> Jenis</a></li>
              </ul>
            </li>
            <br>
            <br>
            <br>
            <br>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="logout.php"><i class="fa fa-circle-o"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
        </section>
      </aside>
      <aside class="main">
        <div class="row">
                <!--/span-->
                <div class="col-lg-9" id="content">
                    <div class="row-fluid">
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	</div>
                    	</div>
                        <div class="row-fluid">
                        <!-- block -->
					
							
                        <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Table Peminjaman</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
                                <div class="panel-body">
                                    <div class="table-responsive">
            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Status Peminjaman</th>
                            <th>Pegawai</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
        include 'koneksi.php';
        $no = 1;
        $select = mysqli_query($koneksi,"select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai ORDER BY id DESC");
        while($data = mysqli_fetch_array($select)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['tanggal_pinjam'] ?></td>
                            <td><?php echo $data['tanggal_kembali'] ?></td>
                            <td><?php echo $data['status_peminjaman'] ?></td>
                            <td><?php echo $data['nama_pegawai'] ?></td>
                            <td><a class="btn btn-info " href="edit_pengembalian.php?id=<?php echo $data['id'] ?>">Kembalikan</a>
                            <a type="button" class="btn btn-danger fa fa-trash" href="hapus_pengembalian.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td>
                        </tr>
                        <?php 
        }
        ?>
                </table>
                <br><br>
                <h2>Tabel Data Peminjaman Detail</h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="example2">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
        include 'koneksi.php';
        $no = 1;
            $select = mysqli_query($koneksi,"select peminjaman_detail.jumlah, inventaris.nama, peminjaman_detail.id_peminjaman from peminjaman_detail 
                JOIN inventaris ON peminjaman_detail.id_inventaris = inventaris.id_inventaris JOIN peminjaman ON peminjaman_detail.id_peminjaman = peminjaman.id");
        while($data = mysqli_fetch_array($select)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['nama'] ?></td>
                            <td><?php echo $data['jumlah'] ?></td>
                            <td>
                            <center><a type="button" class="btn btn-danger fa fa-trash" href="hapus_peminjaman_detail.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td></center>
                        </tr>
                        <?php 
        }
        ?>
                </table>
                <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example').DataTable();
                                        });
                                        </script>
                                                        <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example2').DataTable();
                                        });
                                        </script>

            </div>
            </div>
            </div>
            </div>
            </div>

                     <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>