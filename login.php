<?php

include ('koneksi.php');


if(!isset($_SESSION)){
	session_start();
}

if(isset($_POST['username'])){
	$username = mysqli_real_escape_string($koneksi, addslashes(trim($_POST['username'])));
	$password = mysqli_real_escape_string($koneksi,trim($_POST['password']));

	$query = mysqli_query($koneksi, "SELECT * FROM petugas WHERE username = '$username' AND password = '$password'");
	$data = mysqli_fetch_array($query);
	$cek=mysqli_num_rows($query);
	if ($cek>0)
	{
		$_SESSION['username'] = $username;
		$_SESSION['petugas'] = $data['nama_petugas'];
		$_SESSION['id_level'] =  $data['id_level'];
		header("location:successlogin.php");
	}
}
	
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
<body>
	<br>
	<br><br><br><br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-lg-2">'
			</div>
			<div class="col-lg-8" align="center">
				<div class="panel panel-default">
					<div class="panel-content">
						<form action="" method="post" width="100%">
				
							<h1>Login Admin</h1>
							<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-8">
										<div class="form-group">
											<label for="username">username</label>
											<input class="form-control" type="text" pattern="[A-Za-z]{8,0}" id="username" autocomplete="off" name="username" value="" placeholder="username" maxlength="20" class="login username-field" />
										</div> 
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2"></div>
										<div class="col-lg-8">
											
											<div class="form-group">
												<label for="password">Password:</label>
												<input class="form-control" type="password" id="password" name="password" value="" placeholder="Password" maxlength="20" class="login password-field"/>
											</div>
										</div>
									</div>
								</div>															
													
								<button class="button btn btn-success btn-large" style="margin-bottom: 33px;">Sign In</button><br>
								<a href="user/login.php">Login sebagai member</a><br>
								<a href="pegawai/login.php">Login sebagai pegawai</a>
								<?php
								if (isset($cek) && (!$cek))
								{
									echo 'Login gagal, username atau password salah<br><br>';
								}
								?>
								
						</form>
					</div>
					<div style="height: 30px"></div>
				</div>
			</div>
		</div>
	</div>
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
