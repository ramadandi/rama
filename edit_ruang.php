
<?php
include "koneksi.php";
$id_ruang=$_GET['id_ruang'];

$select=mysqli_query($koneksi,"select * from ruang where id_ruang='$id_ruang'");
$data=mysqli_fetch_array($select);
?>
<?php
include ('cek.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
                    <div class="span8">
                    <div class="content">

                        <div class="module">
                            <div class="module-head">
                                <h3>Forms</h3>
                            </div>
                
                        <!-- block -->
                        <br>    
                            <div class="block-content collapse in">
                                <div class="content">
                                  <form action="update_ruang.php" method="post" class="form-horizontal">
                                      <fieldset>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Nama ruang</label>
                                          <div class="controls">
                                            <input name="nama_ruang" type="text"  placeholder="" class="span6" id="typeahead" 
                                             value="<?php echo $data['nama_ruang'];?>"
                                             data-provide="typeahead" 
                                            data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kode ruang</label>
                                          <div class="controls">
                                            <input name="kode_ruang" type="text"  id="typeahead"  
                                               value="<?php echo $data['kode_ruang'];?>"
                                            data-provide="typeahead"
                                            data-items="4" >
                                            </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Keterangan </label>
                                          <div class="controls">
                                            <input name="keterangan" type="text"  placeholder="" class="span6" id="typeahead"
                                               value="<?php echo $data['keterangan'];?>"
                                              data-provide="typeahead" 
                                            data-items="4" >
                                            </div>
                                        </div>

                                        <br>
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                       
                                      </fieldset>
                                    </form>
                                </div>
                            </div>
                            </div>

                        
                   <!--/.wrapper-->
                   <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
          <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>
</body>
