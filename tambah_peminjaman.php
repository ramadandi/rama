<?php
include ('cek.php');
include ('koneksi.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris Barang </a>
                    
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>
                        <ul class="nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php 
                             echo $_SESSION['petugas']?>
                                <b class="caret"></b></a>
                               
                            </li>
                        
                              <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                    <?php
                    if ($_SESSION['id_level']==1){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="inventaris.php"><i class="menu-icon icon-dashboard"></i>Inventaris
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman </a>
                                </li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-inbox"></i>Pengembalian <b class="label green pull-right">
                                    </b> </a></li>
                                <li><a href="ruang.php"><i class="menu-icon icon-tasks"></i>Ruang<b class="label orange pull-right">
                                    </b> </a></li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                            <ul class="widget widget-menu unstyled">
                                <li><a href="pegawai.php"><i class="menu-icon icon-bold"></i> Pegawai</a></li>
                                <li><a href="jenis.php"><i class="menu-icon icon-book"></i>Jenis </a></li>
                            </ul>
                    </div>';
                }
                        elseif ($_SESSION['id_level']==2){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-bullhorn"></i>Pengembalian</a>
                                </li>
                            </ul>
                    </div>';
                }
                       elseif ($_SESSION['id_level']==3){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                
                            </ul>
                    </div>';
                }
                ?>
        
                        <div class="span8">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Forms</h3>
							</div>
				
                        <!-- block -->
                       	<br>	
                            <div class="block-content collapse in">
                                <div class="content">
                                  <form action="simpan_peminjaman.php" method="post" class="form-horizontal">
                                      <fieldset>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Id peminjaman </label>
                                          <div class="controls">
                                            <input name="id_peminjaman" type="text"  placeholder="" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                       
                                        </div>
										<div class="control-group">
										<label class="control-label">Nama Barang</label>
											<div class="controls">
												<select name="id_inventaris" class="span6">
											<?php  
												$inventaris = mysql_query("SELECT * FROM inventaris");
												while($data = mysql_fetch_array($inventaris)){ ?>
														<option value="<?php echo $data['id_inventaris'] ?>"><?php echo $data['nama'] ?></option>
											<?php } ?>
												
											</select>
											</div>
										</div>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Tanggal peminjaman </label>
                                          <div class="controls">
                                            <input name="tanggal_pinjam" type="date"  id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Tanggal pengembalian </label>
                                          <div class="controls">
                                            <input name="tanggal_kembali" type="date"  id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
										
                                        
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Status Peminjaman </label>
                                          <div class="controls">
                                            <input name="status_peminjaman" type="text"  placeholder="" class="span6" id="typeahead"  data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                        </div>
                                         <div class="control-group">
                                          <label class="control-label" for="typeahead">Pegawai </label>
                                          <div class="controls">
                                          <select name="id_pegawai"  class="span6" class="form-control"> 
                    <?php
                                            
                                             include "koneksi.php";
                  $select = mysql_query("SELECT * FROM pegawai");
                  while($data = mysql_fetch_array($select))
                  {
                    ?>
                    <center>
                      <option value='<?php echo $data['id_pegawai'];?>'><?php echo $data['nama_pegawai'];?></option>
                    </center>
                    <?php } ?>
             </select>
                                            </div>
                                        </div>
                                    <br>
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="reset" class="btn btn-danger">Reset</button>
                                       
                                      </fieldset>
                                    </form>
                                </div>
							</div>
                            </div>

						
			       <!--/.wrapper-->
                  <div class="footer">
            <div class="container">
                <b class="copyright">
            </div>
        </div>
          <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>
</body>
