<?php
include ('cek.php');
include ('koneksi.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
      
      <header class="main-header">
        <!-- Logo -->
        <a class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Inventaris</b></span>
        </a>
        <ul class="nav pull-right">
                        
    </ul>
        <!-- Header Navbar: style can be found in header.less -->

              <!-- Control Sidebar Toggle Button -->
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Menu admin</p>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
          
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="inventaris.php"><i class="fa fa-circle-o"></i> Inventaris</a></li>
              </ul>
            </li>
 <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="peminjaman.php"><i class="fa fa-circle-o"></i> Peminjaman</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pengembalian.php"><i class="fa fa-circle-o"></i> Pengembalian</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="ruang.php"><i class="fa fa-circle-o"></i> Ruang</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="pegawai.php"><i class="fa fa-circle-o"></i> Pegawai</a></li>
              </ul>
            </li>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="jenis.php"><i class="fa fa-circle-o"></i> Jenis</a></li>
              </ul>
            </li>
            <br>
            <br>
            <br>
            <br>
            <li class="active treeview">
              <ul class="treeview-menu">
                <li class="active"><a href="logout.php"><i class="fa fa-circle-o"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
        </section>
      </aside>
      <aside class="main">
        <div class="row">
        <!-- <div class="col-lg-3"></div> -->
        <div class="col-lg-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                            Form Peminjaman                       
											</div>
                                            <div class="right_col" role="main">
                                            <div class="right_col" role="main">
        <div class="col-md-1"></div>
        <div class="col-md-10">
                <form class="form-inline" action="" method="GET" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Nama Pegawai</label>
                    <select class="form-control" tabindex="-1" name="id_pegawai" >
                        <?php
                        $select = mysqli_query($koneksi,"SELECT * FROM pegawai");
                        // var_dump($select2);die();
                        ?>
                        <?php while($a = mysqli_fetch_array($select)){ ?>
                            <option value="<?php echo $a['id_pegawai'] ?>" <?=isset($_GET['id_pegawai']) ? ($a['id_pegawai'] == $_GET['id_pegawai'] ? 'selected' :'') : '' ?>><?php echo $a['nama_pegawai']?> </option>
                        <?php } ?>
                    </select>
                </div>
                <center><div class="form-group">
                    <label>Nama Barang</label>
                    <select class="form-control" tabindex="-1" name="id_inventaris" >
                        <?php
                        $select = mysqli_query($koneksi,"SELECT * FROM inventaris");
                        ?>
                        <?php while($b = mysqli_fetch_array($select)){ ?>
                            <option value="<?php echo $b['id_inventaris'] ?>"><?php echo $b['nama']?> </option>
                        <?php }?>
                    </select>
                </div></center><br>
                <center>
                    <button class="btn btn-Danger" type="submit">Cek Barang</button>
                </center>
            </form>
            <?php
            if(isset($_GET['id_pegawai']) && isset($_GET['id_inventaris'])){?>
            <form action="proses_peminjaman.php" method="post" enctype="multipart/form-data">
               <?php
               include "koneksi.php";
               $id_inventaris=$_GET['id_inventaris'];
               $select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
               $data=mysqli_fetch_array($select);
                ?>
                <br>
                <br>
                <div class="row">
                  <input name="id_pegawai" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $_GET['id_pegawai'];?>" autocomplete="off" maxlength="11" required="">
                  <input name="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="">
                  <div class="span3">Nama<input name="nama" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" readonly></div>
                  <div class="span3">Jumlah Tersedia<input name="jumlah" type="text" class="form-control" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly></div>
                  <div class="span3">Jumlah Pinjam<input name="jumlah_pinjam" type="number" class="form-control" placeholder="Jumlah" autocomplete="off" min="1" max="<?php echo $data['jumlah'];?>"></div>
                  <br><br><br>
                  <br><button type="submit" class="btn">Pinjam</button>
                  <br>
                  <br>
                </div>
              </form>

            <?php 
            $status_peminjaman="Pinjam";
            ?>
            <br><br>
            <div class="row">
              <div class="panel-body">
                  <div class="table-responsive">

                    <div class="col-lg-12">
                      <?php 
                      $tanggal_pinjam=date('Y-m-d H:i:s'); 
                      ?>
                      <div class="row" style="margin-left: 550px">
                        <div class="col-md-7">Tanggal Pinjam<input name="tanggal_pinjam" readonly type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $tanggal_pinjam;?>" autocomplete="off" maxlength="11"></div>
                    </div><br>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Nama Pegawai</th>
                                <th>Option</th>

                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select=mysqli_query($koneksi,"select t.*,p.*,i.*,t.jumlah from temp_peminjaman t JOIN inventaris i ON t.id_inventaris=i.id_inventaris JOIN pegawai p ON t.id_pegawai=p.id_pegawai where t.id_pegawai='$_GET[id_pegawai]'");
                            while($data=mysqli_fetch_array($select))
                            {
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$data['nama']?></td>
                                    <td><?=$data['jumlah']?></td>
                                    <td><?=$data['nama_pegawai']?></td>
                                    <td>
                                    <center><a class="btn btn outline btn-info  glyphicon glyphicon-trash" href="hapus_temp_peminjaman.php?id=<?php echo $data['id'] ?>&id_pegawai=<?php echo $_GET['id_pegawai'] ?>&id_inventaris=<?php echo $_GET['id_inventaris'] ?>"><b>X</b></a></td></center>

                               </tr>

                                       <?php
                                   }
                                   ?>

                           </tbody>
                       </table>
                       <br>
                       <a href="checkout.php?id_pegawai=<?=$_GET['id_pegawai']?>" class="btn btn-warning">&nbsp;Pinjam</a>
                       <hr><br>
                       <br>

                <br>

            <br/>

        </div>
    </div>
    </div>
    </div>
              <?php } ?>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
            <hr>
            <footer>
                <center>
                    <p>&copy; Inventory Sekolah@ 2019</p>
                </center>
            </footer>
        </div>
        </div>
      </aside>
                 
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="index.php">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                    <li class="active">Peminjaman</li>
	                                </ul>
                            	</div>
                        	
                        
	
                
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


<script src="dist/js/bootstrap.js"></script>




<script src="assets2/js/vendor/holder.min.js"></script>

<script src="assets2/js/vendor/ZeroClipboard.min.js"></script>

<script src="assets2/js/vendor/anchor.js"></script>

<script src="assets2/js/src/application.js"></script>





<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets2/js/ie10-viewport-bug-workaround.js"></script>


<script>
window.twttr = (function (d,s,id) {
  var t, js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return; js=d.createElement(s); js.id=id; js.async=1;
  js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
  return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
}(document, "script", "twitter-wjs"));
</script>

<!-- Analytics
================================================== -->
<script>
var _gauges = _gauges || [];
(function() {
  var t   = document.createElement('script');
  t.async = true;
  t.id    = 'gauges-tracker';
  t.setAttribute('data-site-id', '4f0dc9fef5a1f55508000013');
  t.src = '//secure.gaug.es/track.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(t, s);
})();
</script>

    </body>
</html>